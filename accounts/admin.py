from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(Profile)
admin.site.register(Company)
admin.site.register(EmployesInCompany)
admin.site.register(Request)
admin.site.register(Tag)
