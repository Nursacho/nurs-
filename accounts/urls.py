from django.urls import path, include
from .views import *

urlpatterns = [
    path('index/', index),
    path('register/', UserRegisterView.as_view(), name='register'),
    path('', include('django.contrib.auth.urls')),
    path('profile/', profile, name='profile'),
]
